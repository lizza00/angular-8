import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MyComponent} from './my/my.component';
import {BankComponent} from './bank/bank.component';
import {FormsModule} from '@angular/forms';
import {NewBankComponent} from './new-bank/new-bank.component';
import {BankListComponent} from './bank-list/bank-list.component';
import {ClientListComponent} from './client-list/client-list.component';
import {RouterModule, Routes} from '@angular/router';
import { ClientComponent } from './client/client.component';
import { NewClientComponent } from './new-client/new-client.component';

const routes: Routes = [
  {path: 'banks', component: BankListComponent},
  {path: 'clients/:bankId', component: ClientListComponent},
  {path: '', redirectTo: 'banks', pathMatch: 'full'},

];

@NgModule({
  declarations: [
    AppComponent,
    MyComponent,
    BankComponent,
    NewBankComponent,
    BankListComponent,
    ClientListComponent,
    ClientComponent,
    NewClientComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
