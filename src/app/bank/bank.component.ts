import {Component, Input, OnInit, Output} from '@angular/core';
import {BanksDataService} from '../services/banks-data.service';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {

  showInfo = false;
  @Input() bank;
  @Input() bankIndex;

  constructor(private banksDataService: BanksDataService) {
  }

  ngOnInit() {
  }

  removeBank() {
    this.banksDataService.removeBank(this.bankIndex);
  }
}
