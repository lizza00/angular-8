import {Component, OnInit} from '@angular/core';
import {BanksDataService} from '../services/banks-data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

  clients: any[];

  constructor(private banksDataService: BanksDataService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => this.getClients(params.bankId));
  }

  ngOnInit() {

  }

  getClients(bankId: string) {
    console.log('cl');
    this.banksDataService.getClients(bankId).subscribe(clients => this.clients = clients);
  }

  onChange(event) {
    this.activatedRoute.params.subscribe(params => this.getClients(params.bankId));
  }
}
