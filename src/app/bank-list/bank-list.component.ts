import {Component, OnInit} from '@angular/core';
import {BanksDataService} from '../services/banks-data.service';

@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.scss']
})
export class BankListComponent implements OnInit {


  banks: any[];

  ngOnInit() {
  }

  constructor(private banksDataService: BanksDataService) {
    console.log('b')
    banksDataService.getBanks().subscribe((banks) => this.banks = banks);
  }
}
