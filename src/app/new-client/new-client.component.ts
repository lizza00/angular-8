import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BanksDataService} from '../services/banks-data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.component.html',
  styleUrls: ['./new-client.component.scss']
})
export class NewClientComponent implements OnInit {

  @Output() change = new EventEmitter();
  showForm = false;

  constructor(private banksDataService: BanksDataService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
  }

  onSubmit(myForm) {
    const fields = myForm.form.controls;
    this.showForm = false;
    this.activatedRoute.params.subscribe(params =>
      this.banksDataService.addClient({
        name: fields.name.value,
        bankId: params.bankId
      }));
    this.change.emit(true);
  }
}
