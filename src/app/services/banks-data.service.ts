import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BanksDataService {

  constructor() {
  }

  banks = [
    {
      name: 'Privat',
      budget: 1213,
      location: `Ukraine`
    },
    {
      name: 'Mono',
      budget: 213,
      location: `Latvia`
    }
  ];

  private clients = [
    {name: 'Alex Kovalsyi', bankId: 'Privat'},
    {name: 'John Malkovych', bankId: 'Privat'},
    {name: 'David Putch', bankId: 'Privat'},
    {name: 'Kiril 11', bankId: 'Mono'},
    {name: 'Alex Romanovskyi', bankId: 'Mono'},
    {name: 'Sergii Kovalsyi', bankId: 'Mono'},
  ];

  addBank(bank) {
    this.banks.push(bank);
  }

  removeBank(index) {
    this.banks.splice(index, 1);
  }

  getBanks(): Observable<any[]> {
    return of(this.banks);
  }

  getClients(bankId: string): Observable<any[]> {
    return of(this.clients.filter(c => c.bankId === bankId));
  }

  removeClient(index) {
    this.clients.splice(index, 1);
  }

  addClient(client) {
    this.clients.push(client);
  }
}
