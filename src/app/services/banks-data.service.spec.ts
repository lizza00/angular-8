import { TestBed } from '@angular/core/testing';

import { BanksDataService } from './banks-data.service';

describe('BanksDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BanksDataService = TestBed.get(BanksDataService);
    expect(service).toBeTruthy();
  });
});
