import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BanksDataService} from '../services/banks-data.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  @Input() client;
  @Input() clientIndex;
  showInfo = false;
  @Output() change = new EventEmitter();

  constructor(private banksDataService: BanksDataService) {
  }

  ngOnInit() {
  }

  removeClient() {
    this.banksDataService.removeClient(this.clientIndex);
    this.change.emit(true);
  }
}
