import {Component, OnInit} from '@angular/core';
import {BanksDataService} from '../services/banks-data.service';

@Component({
  selector: 'app-new-bank',
  templateUrl: './new-bank.component.html',
  styleUrls: ['./new-bank.component.scss']
})
export class NewBankComponent implements OnInit {

  showForm = false;

  constructor(private banksDataService: BanksDataService) {
  }

  ngOnInit() {
  }

  onSubmit(myForm) {
    const fields = myForm.form.controls;
    this.showForm = false;
    this.banksDataService.addBank({
      name: fields.name.value,
      budget: fields.budget.value,
      location: fields.location.value
    });
  }

}
